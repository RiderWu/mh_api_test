<?xml version="1.0" encoding="UTF-8"?>
<company>
<details>
<name>Millennium &amp; Copthorne Hotels PLC</name>
</details>
<news id="2588344861065216">
<when>
<day>02</day>
<month>09</month>
<year>2016</year>
<hour>16</hour>
<min>40</min>
<sec>00</sec>
</when>
<headline>Second Price Monitoring Extn</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2588344861065216</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2588344861065216</link_xml>
</news>
<news id="2588338418614272">
<when>
<day>02</day>
<month>09</month>
<year>2016</year>
<hour>16</hour>
<min>35</min>
<sec>00</sec>
</when>
<headline>Price Monitoring Extension</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2588338418614272</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2588338418614272</link_xml>
</news>
<news id="2586560302153728">
<when>
<day>01</day>
<month>09</month>
<year>2016</year>
<hour>12</hour>
<min>41</min>
<sec>00</sec>
</when>
<headline>Total Voting Rights</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2586560302153728</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2586560302153728</link_xml>
</news>
<news id="2580601035030528">
<when>
<day>24</day>
<month>08</month>
<year>2016</year>
<hour>17</hour>
<min>22</min>
<sec>00</sec>
</when>
<headline>Directorate Change - Retirement of Group CEO</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2580601035030528</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2580601035030528</link_xml>
</news>
<news id="2574983217807360">
<when>
<day>18</day>
<month>08</month>
<year>2016</year>
<hour>10</hour>
<min>34</min>
<sec>00</sec>
</when>
<headline>Appointment of Independent Non-Executive Director</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2574983217807360</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2574983217807360</link_xml>
</news>
<news id="2556824096079872">
<when>
<day>03</day>
<month>08</month>
<year>2016</year>
<hour>17</hour>
<min>06</min>
<sec>00</sec>
</when>
<headline>Interim Dividend</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2556824096079872</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2556824096079872</link_xml>
</news>
<news id="2555323005009920">
<when>
<day>03</day>
<month>08</month>
<year>2016</year>
<hour>07</hour>
<min>00</min>
<sec>00</sec>
</when>
<headline>Half-year Report</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2555323005009920</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2555323005009920</link_xml>
</news>
<news id="2552907085905920">
<when>
<day>01</day>
<month>08</month>
<year>2016</year>
<hour>14</hour>
<min>24</min>
<sec>00</sec>
</when>
<headline>Voting Rights and Capital</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2552907085905920</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2552907085905920</link_xml>
</news>
<news id="2545337206046720">
<when>
<day>26</day>
<month>07</month>
<year>2016</year>
<hour>17</hour>
<min>05</min>
<sec>00</sec>
</when>
<headline>Disclosure of Interests in Shares</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2545337206046720</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2545337206046720</link_xml>
</news>
<news id="2536839613251584">
<when>
<day>19</day>
<month>07</month>
<year>2016</year>
<hour>09</hour>
<min>23</min>
<sec>00</sec>
</when>
<headline>Director Declaration - Additional Directorship</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2536839613251584</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2536839613251584</link_xml>
</news>
<news id="2526192389324800">
<when>
<day>07</day>
<month>07</month>
<year>2016</year>
<hour>16</hour>
<min>35</min>
<sec>00</sec>
</when>
<headline>Price Monitoring Extension</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2526192389324800</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2526192389324800</link_xml>
</news>
<news id="2517095648591872">
<when>
<day>01</day>
<month>07</month>
<year>2016</year>
<hour>11</hour>
<min>00</min>
<sec>00</sec>
</when>
<headline>Appointment of Group Chief Financial Officer</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2517095648591872</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2517095648591872</link_xml>
</news>
<news id="2516883047710720">
<when>
<day>01</day>
<month>07</month>
<year>2016</year>
<hour>10</hour>
<min>16</min>
<sec>00</sec>
</when>
<headline>Total Voting Rights</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2516883047710720</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2516883047710720</link_xml>
</news>
<news id="2482471769735168">
<when>
<day>01</day>
<month>06</month>
<year>2016</year>
<hour>12</hour>
<min>10</min>
<sec>00</sec>
</when>
<headline>Total Voting Rights</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2482471769735168</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2482471769735168</link_xml>
</news>
<news id="2455772105539584">
<when>
<day>05</day>
<month>05</month>
<year>2016</year>
<hour>17</hour>
<min>14</min>
<sec>00</sec>
</when>
<headline>Result of AGM</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2455772105539584</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2455772105539584</link_xml>
</news>
<news id="2454225917313024">
<when>
<day>05</day>
<month>05</month>
<year>2016</year>
<hour>06</hour>
<min>00</min>
<sec>00</sec>
</when>
<headline>1st Quarter Results</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2454225917313024</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2454225917313024</link_xml>
</news>
<news id="2452099908501504">
<when>
<day>03</day>
<month>05</month>
<year>2016</year>
<hour>15</hour>
<min>41</min>
<sec>00</sec>
</when>
<headline>Voting Rights and Capital</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2452099908501504</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2452099908501504</link_xml>
</news>
<news id="2436466227544064">
<when>
<day>20</day>
<month>04</month>
<year>2016</year>
<hour>16</hour>
<min>15</min>
<sec>00</sec>
</when>
<headline>Director Declaration</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2436466227544064</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2436466227544064</link_xml>
</news>
<news id="2429733866307584">
<when>
<day>14</day>
<month>04</month>
<year>2016</year>
<hour>14</hour>
<min>59</min>
<sec>00</sec>
</when>
<headline>Block listing Interim Review</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2429733866307584</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2429733866307584</link_xml>
</news>
<news id="2414229034369024">
<when>
<day>01</day>
<month>04</month>
<year>2016</year>
<hour>16</hour>
<min>58</min>
<sec>00</sec>
</when>
<headline>Annual Report and Accounts 2015</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2414229034369024</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2414229034369024</link_xml>
</news>
<news id="2413984221233152">
<when>
<day>01</day>
<month>04</month>
<year>2016</year>
<hour>15</hour>
<min>15</min>
<sec>00</sec>
</when>
<headline>Board Committee Changes</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2413984221233152</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2413984221233152</link_xml>
</news>
<news id="2412605536731136">
<when>
<day>01</day>
<month>04</month>
<year>2016</year>
<hour>10</hour>
<min>29</min>
<sec>00</sec>
</when>
<headline>Voting Rights and Capital</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2412605536731136</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2412605536731136</link_xml>
</news>
<news id="2409629124395008">
<when>
<day>30</day>
<month>03</month>
<year>2016</year>
<hour>16</hour>
<min>30</min>
<sec>00</sec>
</when>
<headline>Director/PDMR Shareholding</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2409629124395008</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2409629124395008</link_xml>
</news>
<news id="2375151274426368">
<when>
<day>01</day>
<month>03</month>
<year>2016</year>
<hour>10</hour>
<min>30</min>
<sec>26</sec>
</when>
<headline>Voting Rights and Capital</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2375151274426368</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2375151274426368</link_xml>
</news>
<news id="2364416003670016">
<when>
<day>19</day>
<month>02</month>
<year>2016</year>
<hour>07</hour>
<min>00</min>
<sec>06</sec>
</when>
<headline>Final Results 2015</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2364416003670016</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2364416003670016</link_xml>
</news>
<news id="2357745919459328">
<when>
<day>12</day>
<month>02</month>
<year>2016</year>
<hour>07</hour>
<min>00</min>
<sec>05</sec>
</when>
<headline>Impairment of Assets</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2357745919459328</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2357745919459328</link_xml>
</news>
<news id="2346533907333120">
<when>
<day>01</day>
<month>02</month>
<year>2016</year>
<hour>14</hour>
<min>50</min>
<sec>52</sec>
</when>
<headline>Voting Rights and Capital</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2346533907333120</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2346533907333120</link_xml>
</news>
<news id="2343166652973056">
<when>
<day>28</day>
<month>01</month>
<year>2016</year>
<hour>16</hour>
<min>44</min>
<sec>55</sec>
</when>
<headline>Disclosure of Interests in Shares</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2343166652973056</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2343166652973056</link_xml>
</news>
<news id="2326974626267136">
<when>
<day>13</day>
<month>01</month>
<year>2016</year>
<hour>17</hour>
<min>20</min>
<sec>16</sec>
</when>
<headline>Disclosure of Interests in Shares</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2326974626267136</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2326974626267136</link_xml>
</news>
<news id="2314235753267200">
<when>
<day>04</day>
<month>01</month>
<year>2016</year>
<hour>09</hour>
<min>51</min>
<sec>22</sec>
</when>
<headline>Voting Rights and Capital</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2314235753267200</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2314235753267200</link_xml>
</news>
<news id="2278357743960064">
<when>
<day>01</day>
<month>12</month>
<year>2015</year>
<hour>10</hour>
<min>38</min>
<sec>58</sec>
</when>
<headline>Total Voting Rights</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2278357743960064</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2278357743960064</link_xml>
</news>
<news id="2243465429647360">
<when>
<day>02</day>
<month>11</month>
<year>2015</year>
<hour>11</hour>
<min>00</min>
<sec>40</sec>
</when>
<headline>Voting Rights and Capital</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2243465429647360</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2243465429647360</link_xml>
</news>
<news id="2239634318819328">
<when>
<day>29</day>
<month>10</month>
<year>2015</year>
<hour>07</hour>
<min>00</min>
<sec>08</sec>
</when>
<headline>3rd Quarter Results</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2239634318819328</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2239634318819328</link_xml>
</news>
<news id="2225166721482752">
<when>
<day>14</day>
<month>10</month>
<year>2015</year>
<hour>15</hour>
<min>04</min>
<sec>26</sec>
</when>
<headline>Blocklisting Long Term Incentive Plan</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2225166721482752</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2225166721482752</link_xml>
</news>
<news id="2225121624326144">
<when>
<day>14</day>
<month>10</month>
<year>2015</year>
<hour>14</hour>
<min>59</min>
<sec>20</sec>
</when>
<headline>Blocklisting Interim Review</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2225121624326144</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2225121624326144</link_xml>
</news>
<news id="2225128066777088">
<when>
<day>14</day>
<month>10</month>
<year>2015</year>
<hour>14</hour>
<min>58</min>
<sec>25</sec>
</when>
<headline>Block Listing Cancellation</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2225128066777088</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2225128066777088</link_xml>
</news>
<news id="2210157958266880">
<when>
<day>01</day>
<month>10</month>
<year>2015</year>
<hour>11</hour>
<min>09</min>
<sec>36</sec>
</when>
<headline>Total Voting Rights</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2210157958266880</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2210157958266880</link_xml>
</news>
<news id="2188661646950400">
<when>
<day>10</day>
<month>09</month>
<year>2015</year>
<hour>16</hour>
<min>32</min>
<sec>53</sec>
</when>
<headline>Disclosure of Interests in Shares</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2188661646950400</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2188661646950400</link_xml>
</news>
<news id="2177591368744960">
<when>
<day>01</day>
<month>09</month>
<year>2015</year>
<hour>10</hour>
<min>10</min>
<sec>06</sec>
</when>
<headline>Total Voting Rights</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2177591368744960</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2177591368744960</link_xml>
</news>
<news id="2174127477620736">
<when>
<day>27</day>
<month>08</month>
<year>2015</year>
<hour>11</hour>
<min>04</min>
<sec>44</sec>
</when>
<headline>Disclosure of Interests in Shares</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2174127477620736</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2174127477620736</link_xml>
</news>
<news id="2171803900313600">
<when>
<day>25</day>
<month>08</month>
<year>2015</year>
<hour>09</hour>
<min>49</min>
<sec>09</sec>
</when>
<headline>Disclosure of Interests in Shares</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2171803900313600</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2171803900313600</link_xml>
</news>
<news id="2159747927113728">
<when>
<day>07</day>
<month>08</month>
<year>2015</year>
<hour>17</hour>
<min>29</min>
<sec>51</sec>
</when>
<headline>Acquisition</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2159747927113728</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2159747927113728</link_xml>
</news>
<news id="2150610384191488">
<when>
<day>04</day>
<month>08</month>
<year>2015</year>
<hour>14</hour>
<min>46</min>
<sec>16</sec>
</when>
<headline>Director/PDMR Shareholding</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2150610384191488</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2150610384191488</link_xml>
</news>
<news id="2150487977623552">
<when>
<day>04</day>
<month>08</month>
<year>2015</year>
<hour>13</hour>
<min>56</min>
<sec>44</sec>
</when>
<headline>Total Voting Rights</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2150487977623552</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2150487977623552</link_xml>
</news>
<news id="2145542322782208">
<when>
<day>30</day>
<month>07</month>
<year>2015</year>
<hour>07</hour>
<min>01</min>
<sec>57</sec>
</when>
<headline>Half Yearly Report</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2145542322782208</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2145542322782208</link_xml>
</news>
<news id="2121228512919552">
<when>
<day>01</day>
<month>07</month>
<year>2015</year>
<hour>09</hour>
<min>31</min>
<sec>46</sec>
</when>
<headline>Voting Rights and Capital</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2121228512919552</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2121228512919552</link_xml>
</news>
<news id="2086565979357184">
<when>
<day>01</day>
<month>06</month>
<year>2015</year>
<hour>11</hour>
<min>20</min>
<sec>40</sec>
</when>
<headline>Voting Rights and Capital</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2086565979357184</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2086565979357184</link_xml>
</news>
<news id="2074477793902592">
<when>
<day>18</day>
<month>05</month>
<year>2015</year>
<hour>10</hour>
<min>49</min>
<sec>11</sec>
</when>
<headline>Director Declaration</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2074477793902592</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2074477793902592</link_xml>
</news>
<news id="2073964545310720">
<when>
<day>15</day>
<month>05</month>
<year>2015</year>
<hour>15</hour>
<min>51</min>
<sec>00</sec>
</when>
<headline>Disclosure of Interests in Shares</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2073964545310720</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2073964545310720</link_xml>
</news>
<news id="2064927934119936">
<when>
<day>08</day>
<month>05</month>
<year>2015</year>
<hour>15</hour>
<min>27</min>
<sec>19</sec>
</when>
<headline>Director Declaration</headline>
<link>feed-services.jsp?feed=regulatory-news-item&amp;item=2064927934119936</link>
<link_xml>shared/v2/tools/rna/jsp/news-item.xml.jsp?item=2064927934119936</link_xml>
</news>
<info>
        <start_record>1</start_record>
        <end_record>50</end_record>
        <max_restricted>false</max_restricted>
        <using>DATABASE</using>
    </info>
<params>
        <count>50</count>
        <company_id>3055</company_id>
        <market/>
        <epic/>
        <isin/>
        <period/>
        <when>29/09/2016 08:40:47</when>
        <dateFrom/>
        <dateTo/>
        <lower_date/>
        <direction>DOWN</direction>
        <story_source/>
        <story_source_group>RNS</story_source_group>
        <story_type/>
        <category/>
        <exclude_categories/>
        <exclude_story_types/>
        <newsId/>
        <nextAndPrevNews>false</nextAndPrevNews>
        <pageOffset>0</pageOffset>
        <pageNumber>1</pageNumber>
        <instrument_types>Ordinary Share</instrument_types>
    </params>
<legal>
<text>15 minute delayed share price provided by vwd group
        </text>
<disclaimer_link url="http://hsprod.investis.com/common/investis_disclaimer.htm">Disclaimer</disclaimer_link>
</legal>
</company>