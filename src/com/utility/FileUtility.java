package com.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class FileUtility {
	public static String Read(String filePath) {
		String result = "";
		try {
			File file = new File(filePath);
			BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
			String line = null;
			while ((line = bufferedReader.readLine()) != null) {
				result += line;
			}
			bufferedReader.close();
		} catch (Exception e) {
			System.out.println("Open with error " + filePath);
			e.printStackTrace();
		}
		return result;
	}
}
