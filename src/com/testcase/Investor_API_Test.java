package com.testcase;

import org.testng.annotations.Test;

import com.dataprovider.XMLDataProvider;
import com.http.HttpRequest;
import com.utility.FileUtility;

import java.util.List;
import java.util.Map;

import org.custommonkey.xmlunit.DetailedDiff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.XMLUnit;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class Investor_API_Test {

	public static String baseURI = null;

	@BeforeSuite
	public void beforeSuite() {
	}

	@AfterSuite
	public void afterSuite() {
	}

	@BeforeTest
	public void beforeTest() {
	}

	@AfterTest
	public void afterTest() {
	}

	@Parameters({ "URL" })
	@BeforeClass
	public void beforeClass(String URL) {
		baseURI = URL;
	}

	@AfterClass
	public void afterClass() {
	}

	@BeforeMethod
	public void beforeMethod() {
	}

	@AfterMethod
	public void afterMethod() {
	}

	@Test(dataProvider = "dataProvider", dataProviderClass = XMLDataProvider.class)
	public void Test_Get_Feed(Map<?, ?> param) throws Exception {
		String actual = HttpRequest.Get(baseURI, (String) param.get("params"));
		String expected = FileUtility.Read((String) param.get("response"));
		assertXMLEquals(expected, actual);
	}

	public static void assertXMLEquals(String expectedXML, String actualXML) throws Exception {
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreAttributeOrder(true);

		DetailedDiff diff = new DetailedDiff(XMLUnit.compareXML(expectedXML, actualXML));

		List<Difference> allDifferences = diff.getAllDifferences();
		for (int i = 0; i < allDifferences.size(); i++) {
			if (allDifferences.get(i).getTestNodeDetail().getXpathLocation()
					.equals("/company[1]/params[1]/when[1]/text()[1]")) {
				allDifferences.remove(i);
			}
		}
		int diff_count = allDifferences.size();
		Assert.assertEquals(0, diff_count, "Differences found: " + allDifferences.toString());
	}

}
