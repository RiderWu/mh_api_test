package com.http;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HttpRequest {
	public static String Get(String url, String param) throws Exception {
		String result = "";
		BufferedReader br = null;
		HttpURLConnection connection = (HttpURLConnection) new URL(url + "?" + param).openConnection();
		br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line;
        while ((line = br.readLine()) != null) {
            result += line;
        }
        br.close();
        return result;
	}
}
